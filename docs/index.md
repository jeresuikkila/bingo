---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Yet Another Minecraft BINGO"
  image:
    src: https://gitlab.com/horrific-tweaks/bingo/-/raw/main/docs/screenshot.png
    alt: A Minecraft BINGO card
  tagline: A server-side BINGO item hunt mod + lobby, compatible with vanilla clients.
  actions:
    - theme: brand
      text: Get it on Modrinth
      link: https://modrinth.com/mod/yet-another-minecraft-bingo
    - theme: alt
      text: Source Code
      link: https://gitlab.com/horrific-tweaks/bingo

features:
  - icon: ⚙️
    title: Multiple Gamemodes
    details: Choose from "standard", "lockout", or "inventory" modes, with extra options for elytras or PvP!
  - icon: 🏃
    title: Peak Performance
    details: Built to have a minimal impact on the game, with several enhancements to reduce its startup time.
  - icon: 🛠️
    title: Unlimited Customization
    details: Create your own item tiers, change the card images, and play with custom gamerules!
---

