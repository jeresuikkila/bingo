package me.jfenn.bingo.config

enum class BingoMode {
    STANDARD,
    LOCKOUT,
    INVENTORY,
}
