package me.jfenn.bingo.card

import kotlin.test.Test
import kotlin.test.assertEquals

class BingoCardTest {

    @Test
    fun `generates a card of 25 items`() {
        val card = CardService.generate(listOf(5, 5, 5, 5, 5))
        assertEquals(25, card.entries.size)
    }

}